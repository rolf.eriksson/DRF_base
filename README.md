# Proyecto base para tutorial DRF.
                ______      _ _                     _______        _                 _             _           
                |  ____|    (_) |                   |__   __|      | |               | |           (_)          
                | |__   _ __ _| | _____ ___  ___  _ __ | | ___  ___| |__  _ __   ___ | | ___   __ _ _  ___  ___
                |  __| | '__| | |/ / __/ __|/ _ \| '_ \| |/ _ \/ __| '_ \| '_ \ / _ \| |/ _ \ / _` | |/ _ \/ __|
                | |____| |  | |   <\__ \__ \ (_) | | | | |  __/ (__| | | | | | | (_) | | (_) | (_| | |  __/\__ \
                |______|_|  |_|_|\_\___/___/\___/|_| |_|_|\___|\___|_| |_|_| |_|\___/|_|\___/ \__, |_|\___||___/
                                                                                              __/ |            
                                                                                             |___/             


Requiere la siguiente imagen para la maquina virtual de Centos: vagrant box add centos7 https://github.com/holms/vagrant-centos7-box/releases/download/7.1.1503.001/CentOS-7.1.1503-x86_64-netboot.box
